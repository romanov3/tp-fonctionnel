const Tree = value => {

    const Leaf = () => ({
        //isLeaf :: Bool
        isLeaf: () => true,
        join: () => null,
        //left :: Tree a
        left: () => Leaf(),
        //right :: Tree a
        right: () => Leaf(),
        map: () => Leaf(),
        bind: () => EmptyTree.join(),

        //add_left :: Tree a -> Tree a
        add_left: node => Leaf(),
        //add_right :: Tree a -> Tree a
        add_right: node => Leaf(),
        //toString :: String
        toString: () => "f",
    });
    
    const Node = val => left_node => right_node => ({
        //isLeaf :: Bool
        isLeaf: () => false,
        //left :: Tree a
        left: () => left_node,
        //right :: Tree a
        right: () => right_node,
        //join :: Int
        join: () => val,
        map: f => Node(f(val))(left_node.map(f))(right_node.map(f)),
        bind: f => map(f).join(),

        //add_left :: Tree a -> Tree a
        add_left: node => {
            return Node(val)(node)(right_node);
        },
        //add_right :: Tree a -> Tree a
        add_right: node => Node(val)(left_node)(node),
        //toString :: String
        toString: () => {
            return "".concat(val).concat(" (").concat(left_node.toString()).concat(") (").concat(right_node.toString()).concat(")");
        }
    });

    if(value == null || value == undefined){
        return Leaf();
    } else {
        return Node(value)(Leaf())(Leaf());
    }
};

//height :: Tree a -> Int
const height = tree => {
    if(tree.isLeaf()) return 0;
    const leftHeight = 1 + height(tree.right());
    const rightHeight = 1 + height(tree.left());
    if(leftHeight < rightHeight) return rightHeight;
    return leftHeight
}

//size :: Tree a -> Int
const size = tree => {
    if(tree.isLeaf()) return 0;
    return 1 + size(tree.left()) + size(tree.right());
}

//reduce_tree :: (a -> a -> a) -> Tree a -> a -> a 
const reduce_tree = func => tree => acc => {
    if(tree.isLeaf()) return acc;
    return func(reduce_tree(func)(tree.left())(acc))(reduce_tree(func)(tree.right())(acc));
}

//height_2 :: Tree a -> Int
const height_2 = tree => {
    return reduce_tree(x=>y=>(x>y)?x+1:y+1)(tree)(0);
}

//size_2 :: Tree a -> Int
const size_2 = tree => {
    return reduce_tree(x=>y=>x+y+1)(tree)(0);
}

//is_balanced :: Tree a -> Bool
const is_balanced = arbre => {
    return Math.abs(reduce_tree(x=>y=>x+y+1)(arbre.left())(0)-reduce_tree(x=>y=>x+y+1)(arbre.right())(0)) + 1 <= 1;
}

//flattenTree :: Tree a -> [a]
const flattenTree = tree => {
    if(tree.isLeaf()) return [];
    return [].concat(tree.join()).concat(flattenTree(tree.left())).concat(flattenTree(tree.right()));
}

//addValueBTS :: Tree a -> a -> Tree a
const addValueBTS = tree => val => {
    if(tree.isLeaf()) return Tree(val);
    
    if(val <= tree.join()){
        if(tree.left().isLeaf()) return tree.add_left(Tree(val));
        return tree.add_left(addValueBTS(tree.left())(val));
    } else {
        if(tree.right().isLeaf()) return tree.add_right(Tree(val));
        return tree.add_right(addValueBTS(tree.left())(val));
    }
}

//addAllValuesBTS :: Tree a > [a] -> Tree a
const addAllValuesBTS = tree => list => {
    if(list.length == 0) return tree;
    return addAllValuesBTS(addValueBTS(tree)(list[0]))(list.slice(1, list.length));
}

//searchBTS :: Tree a -> a -> Bool
const searchBTS = tree => val => {
    if(tree.isLeaf()) return false;

    if(tree.join() == val) return true;
    if(val <= tree.join()) return searchBTS(tree.left())(val);
    if(val > tree.join()) return searchBTS(tree.right())(val);
}

//sortBTS :: Tree a -> [a]
const sortBTS = tree => {
    if(tree.isLeaf()) return [];
    return [].concat(flattenTree(tree.left())).concat(tree.join()).concat(flattenTree(tree.right()));
}

const my_tree = Tree(5).add_left(Tree(3).add_right(Tree(4))).add_right(Tree(7).add_left(Tree(6)));
const unbalanced_tree = Tree(1).add_left(Tree(2).add_left(Tree(4)).add_right(Tree(5).add_left(Tree(6)))).add_right(Tree(3));
console.log(my_tree.left().toString());
console.log(my_tree.right().toString());
console.log(my_tree.toString());

console.log(height(my_tree));
console.log(size(my_tree));

console.log(height_2(my_tree));
console.log(size_2(my_tree));

console.log(is_balanced(my_tree));
console.log(is_balanced(unbalanced_tree));

console.log(flattenTree(my_tree));

BTS = addValueBTS(Tree())(5);
BTS = addValueBTS(BTS)(2);
BTS = addValueBTS(BTS)(3);
BTS = addValueBTS(BTS)(7);
BTS2 = addAllValuesBTS(Tree())([5,2,3,7]);


console.log(BTS.toString());
console.log(BTS2.toString());

console.log(searchBTS(BTS)(2));
console.log(searchBTS(BTS)(3));
console.log(searchBTS(BTS)(42));

console.log(sortBTS(BTS));